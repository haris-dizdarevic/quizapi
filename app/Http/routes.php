<?php


Route::group(['prefix' => 'api/v1'], function(){
    Route::resource('quizzes', 'api\v1\QuizController',['except' => ['create', 'edit']]);
    Route::resource('questions', 'api\v1\QuestionController',['except' => ['create', 'edit']]);
    Route::resource('answers', 'api\v1\AnswerController',['except' => ['create', 'edit']]);
    Route::resource('categories', 'api\v1\CategoryController',['except' => ['create', 'edit']]);
    Route::resource('results', 'api\v1\ResultController',['except' => ['create', 'edit']]);
    Route::resource('savedQuizzes', 'api\v1\SavedQuizController',['except' => ['create', 'edit']]);
    Route::resource('assignedQuizzes', 'api\v1\AssignedQuizController',['except' => ['create', 'edit']]);
    Route::resource('users', 'api\v1\UserController',['except' => ['create', 'edit']]);
    Route::resource('access', 'api\v1\AccessController', ['only' => ['store', 'destroy']]);

});
