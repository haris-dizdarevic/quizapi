<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use App\Http\Middleware;

class BeforeMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
//        $user = User::where('auth_token', $request->header('Authorization'))->first();
//        if(!$user){
//            return response()->json(['message' => 'You are not logged in'],401 );
//        }
        return $next($request);
    }
}
