<?php

namespace App\Http\Controllers\api\v1;

use App\Models\Answer;
use App\Transformers\AnswerTransformer;
use Illuminate\Http\Request;
use Illuminate\Validation\Validator;
use App\Http\Requests;
use App\Http\Controllers\api\v1\ApiController;
class AnswerController extends ApiController
{
    protected $rules = [
        'answer.question_id' => 'required',
        'answer.answer_text' => 'required',
        'answer.correct' => 'required'
    ];

    function __construct()
    {
        $this->middleware('authToken');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $answers = Answer::all();
        return $this->respond([
            'data' => $answers
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(),$this->rules);
        if($validator->fails()){
            return $this->respondBadRequest();
        }
        $answer = Answer::create($request->get('answer'));
        if(!$answer->save()){
            return $this->respondUnprocessableEntity();
        }
        return $this->respond($answer);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $answer = Answer::findOrFail($id);
        return $this->respond([
            'data' => $answer
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param Request $request
     * @return Response
     */
    public function update($id, Request $request)
    {
        $validator = \Validator::make($request->all(),$this->rules);
        if($validator->fails()){
            return $this->respondBadRequest();
        }
        $answer = Answer::findOrFail($id);
        $answer->update($request->get('answer'));
        if(!$answer->save()){
            return $this->respondUnprocessableEntity();
        }
        return $this->respond([
            'data' => $answer
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $answer = Answer::findOrFail($id);
        $answer->delete();
        return $this->respond(['message' => 'Answer successfully deleted']);
    }
}
