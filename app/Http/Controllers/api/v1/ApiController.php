<?php

namespace App\Http\Controllers\api\v1;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ApiController extends Controller
{

    protected $statusCode = 200;

    /**
     * @return int
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * @param int $statusCode
     */
    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;
        return $this;
    }

    public function respondNotFound($message = 'Not Found'){
        return $this->setStatusCode(404)->respondWithError($message);
    }

    public function respondInternalError($message = 'Internal Server Error'){
        return $this->setStatusCode(500)->respondWithError($message);
    }

    public function respondBadRequest($message = 'Bad Request'){
        return $this->setStatusCode(400)->respondWithError($message);
    }

    public function respondUnprocessableEntity($message = "Unprocessable Entity"){
        return $this->setStatusCode(422)->respondWithError($message);
    }

    public function respondUnauthorize($message = "Unauthorized"){
        return $this->setStatusCode(401)->respondWithError($message);
    }

    public function respond($data, $headers = []){
        return response()->json($data, $this->getStatusCode(), $headers);
    }

    public function respondWithError($message){
        return $this->respond([
            'error' => [
                'message' => $message,
                'status_code' => $this->getStatusCode()
            ]
        ]);
    }
}
