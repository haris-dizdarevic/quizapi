<?php

namespace App\Http\Controllers\api\v1;

use App\Models\User;
use Faker\Provider\Uuid;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class AccessController extends ApiController
{
    protected $rules = [
        'access.username' => 'required',
        'access.password' => 'required'
    ];

    function __construct()
    {
        $this->middleware('cors');
    }


    /**
     * @param Request $request
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), $this->rules);
        if($validator->fails()){
            return $this->respondBadRequest();
        }
        $access = $request->get('access');
        $user = User::where('username', $access['username'])->first();
        if($user->password == $access['password']){
            $token = Uuid::uuid();
            $user->auth_token = $token;
            $user->save();
            return $this->respond($user);
        }
        return $this->respondUnauthorize("Bad Credentials");
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete('auth_token');
        return $this->respond(['message' => 'Successfully logout']);
    }
}
