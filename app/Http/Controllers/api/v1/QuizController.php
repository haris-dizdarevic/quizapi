<?php

namespace App\Http\Controllers\api\v1;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Quiz;
use Illuminate\Validation\Validator;
/**
 * Class QuizController
 * @package App\Http\Controllers\api\v1
 */
class QuizController extends ApiController
{
    /**
     * @var array
     */
    protected $rules = [
        'quiz.user_id' => 'required',
        'quiz.category_id' => 'required',
        'quiz.active' => 'required',
        'quiz.date' => 'required'
    ];

    function __construct()
    {
        $this->middleware('authToken');
    }


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $quizzes = Quiz::all();
        foreach($quizzes as $quiz){
            $quiz->category;
        }
        return $this->respond($quizzes);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(),$this->rules);
        if($validator->fails()){
            return $this->respondBadRequest();
        }
       $newEntry = $request->get('quiz');
        $quiz = Quiz::create($newEntry);
        if(!$quiz->save()){
            return $this->respondUnprocessableEntity();
        }
        return $this->setStatusCode(201)->respond($quiz);
   }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $quiz = Quiz::findOrFail($id);

        foreach($quiz->questions as $question){
        $question->answers;
    }
        return $this->respond($quiz);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param Request $request
     * @return Response
     */
    public function update($id, Request $request)
    {
        $validator = \Validator::make($request->all(),$this->rules);
        if($validator->fails()){
        return $this->respondBadRequest();
    }
        $quiz = Quiz::findOrFail($id);
        $quiz->update($request->get('quiz'));
        if(!$quiz->save()){
            $this->respondUnprocessableEntity();
        }
        return $this->respond([
            'data' => $quiz
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $quiz = Quiz::findOrFail($id);
        $quiz->delete();
        return $this->respond(['message' => 'Succesfully deleted quiz']);
    }
}
