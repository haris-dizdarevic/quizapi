<?php

namespace App\Http\Controllers\api\v1;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\api\v1\ApiController;

class CategoryController extends ApiController
{
    protected $rules =['category.category_name' => 'required'];

    function __construct()
    {
        $this->middleware('authToken');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $categories = Category::all();
        return $this->respond($categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(),$this->rules);
        if($validator->fails()){
            return $this->respondBadRequest();
        }
        $category = Category::create($request->get('category'));
        if(!$category->save()){
            return $this->respondUnprocessableEntity();
        }
        return $this->respond([
            'data' => $this->categoryTransformer->transform($category)
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $category = Category::findOrFail($id);
        return $this->respond([
            'data' => $this->categoryTransformer->transform($category)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param Request $request
     * @return Response
     */
    public function update($id, Request $request)
    {
        $validator = \Validator::make($request->all(),$this->rules);
        if($validator->fails()){
            return $this->respondBadRequest();
        }
        $category = Category::findOrFail($id);
        $category->update($request->get('category'));
        if(!$category->save()){
            return $this->respondUnprocessableEntity();
        }
        return $this->respond([
            'data' => $this->categoryTransformer->transform($category)
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $category = Category::findOrFail($id);
        $category->delete();
        return $this->respond(['message' => 'Category successfully deleted']);
    }
}
