<?php

namespace App\Http\Controllers\api\v1;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Requests;
class UserController extends ApiController
{
    protected $rules = [
        'user.first_name' => 'required',
        'user.last_name' => 'required',
        'user.index_number' => 'required',
        'user.email' => 'required',
        'user.username' => 'required',
        'user.password' => 'required',
        'user.admin' => 'required'
    ];

    function __construct()
    {
        $this->middleware('authToken');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $users = User::all();
        return $this->respond([
            'data' => $users
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(),$this->rules);
        if($validator->fails()){
            return $this->respondBadRequest();
        }
        $user = User::create($request->get('user'));
        if(!$user->save()){
            return $this->respondUnprocessableEntity();
        }
        return $this->respond([
            'data' => $user
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $user = User::findOrFail($id);
        return $this->respond([
            'data' => $user
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param Request $request
     * @return Response
     */
    public function update($id, Request $request)
    {
        $validator = \Validator::make($request->all(),$this->rules);
        if($validator->fails()){
            return $this->respondBadRequest();
        }
        $user = User::findOrFail($id);
        $user->update($request->get('user'));
        if(!$user->save()){
            return $this->respondUnprocessableEntity();
        }
        return $this->respond([
            'data' => $user
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        return $this->respond([
            'message' => 'Successfully deleted user'
        ]);
    }
}
