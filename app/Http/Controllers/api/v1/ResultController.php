<?php

namespace App\Http\Controllers\api\v1;

use App\Models\Result;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ResultController extends ApiController
{
    protected $rules = [
        'result.quiz_id' => 'required',
        'result.user_id' => 'required',
        'result.score' => 'required'
    ];

    function __construct()
    {
        $this->middleware('authToken');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $results = Result::all();
        return $this->respond($results);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(),$this->rules);
        if($validator->fails()){
            return $this->respondBadRequest();
        }
        $result = Result::create($request->get('result'));
        if(!$result->save()){
            return $this->respondUnprocessableEntity();
        }
        return $this->respond( $result);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $result = Result::findOrFail($id);
        return $this->respond([
            'data' => $result
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param Request $request
     * @return Response
     */
    public function update($id, Request $request)
    {
        $validator = \Validator::make($request->all(),$this->rules);
        if($validator->fails()){
            return $this->respondBadRequest();
        }
        $result = Result::findOrFail($id);
        $result->update($request->get('result'));
        if(!$result->save()){
            return $this->respondUnprocessableEntity();
        }
        return $this->respond([
            'data' => $result
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $result = Result::findOrFail($id);
        $result->delete();
        return $this->respond([
            'data' => 'Result successfully deleted'
        ]);
    }
}
