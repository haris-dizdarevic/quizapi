<?php

namespace App\Http\Controllers\api\v1;

use App\Models\Question;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Validation\Validator;
/**
 * Class QuestionController
 * @package App\Http\Controllers\api\v1
 */
class QuestionController extends ApiController
{
    /**
     * @var array
     */
    protected $rules = [
        'question.quiz_id' => 'required',
        'question.question_text' => 'required'
    ];

    function __construct()
    {
        $this->middleware('authToken');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $questions = Question::all();
        return $this->respond([
            'data' => $questions
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(),$this->rules);
        if($validator->fails()){
            return $this->respondBadRequest();
        }
        $question = Question::create($request->get('question'));
        if(!$question->save()){
            return $this->respondUnprocessableEntity();
        }
            return $this->respond($question);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $question = Question::findOrFail($id);
        return $this->respond([
            'data' => $question
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param Request $request
     * @return Response
     */
    public function update($id, Request $request)
    {
        $validator = \Validator::make($request->all(),$this->rules);
        if($validator->fails()){
            return $this->respondBadRequest();
        }
        $question = Question::findOrFail($id);
        $question->update($request->get('question'));
        if(!$question->save()){
            return $this->respondUnprocessableEntity();
        }
        return $this->respond([
            'data' => $question
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $question = Question::findOrFail($id);
        $question->delete();
        return $this->respond([
           'message' => 'Question succesfully deleted'
        ]);
    }
}
