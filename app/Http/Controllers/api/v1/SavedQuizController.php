<?php

namespace App\Http\Controllers\api\v1;

use App\Models\SavedQuiz;
use Illuminate\Http\Request;
use App\Http\Requests;
class SavedQuizController extends ApiController
{
    protected $rules = [
        'savedQuiz.result_id' => 'required',
        'savedQuiz.answer_id' => 'required'
    ];

    function __construct()
    {
        $this->middleware('authToken');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $savedQuizzes = SavedQuiz::all();
        return $this->respond([
            'data' => $savedQuizzes
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @internal param Request $request
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(),$this->rules);
        if($validator->fails()){
            return $this->respondBadRequest();
        }
        $savedQuiz = SavedQuiz::create($request->get('savedQuiz'));
        if(!$savedQuiz->save()){
            return $this->respondUnprocessableEntity();
        }
        return $this->respond($savedQuiz);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $savedQuiz = SavedQuiz::findOrFail($id);
        return $this->respond([
            'data' => $savedQuiz
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param Request $request
     * @return Response
     */
    public function update($id, Request $request)
    {
        $validator = \Validator::make($request->all(),$this->rules);
        if($validator->fails()){
            return $this->respondBadRequest();
        }
        $savedQuiz = SavedQuiz::findOrFail($id);
        $savedQuiz->update($request->get('savedQuiz'));
        if(!$savedQuiz->save()){
            return $this->respondUnprocessableEntity();
        }
        return $this->respond([
            'data' => $savedQuiz
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $savedQuiz = SavedQuiz::findOrFail($id);
        $savedQuiz->delete();
        return $this->respond([
            'message' => 'Quiz successfully deleted'
        ]);
    }
}
