<?php

namespace App\Http\Controllers\api\v1;

use App\Models\AssignedQuiz;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class AssignedQuizController extends ApiController
{
    protected $rules = [
        'assignedQuiz.user_id' => 'required',
        'assignedQuiz.student_id' => 'required',
        'assignedQuiz.quiz_id' => 'required'
    ];

    function __construct()
    {
        $this->middleware('authToken');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $assignedQuizzes = AssignedQuiz::all();
        return $this->respond([$assignedQuizzes]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(),$this->rules);
        if($validator->fails()){
            return $this->respondBadRequest();
        }
        $assignedQuiz = AssignedQuiz::create($request->get('assignedQuiz'));
        if(!$assignedQuiz->save()){
            return $this->respondUnprocessableEntity();
        }
        return $this->respond([$assignedQuiz]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $assignedQuiz = AssignedQuiz::where('student_id',$id)->first();
        $quizzes = $assignedQuiz->quizzes;
        $quizzes->category;

        return $this->respond(array($quizzes));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param Request $request
     * @return Response
     */
    public function update($id, Request $request)
    {
        $validator = \Validator::make($request->all(),$this->rules);
        if($validator->fails()){
            return $this->respondBadRequest();
        }
        $assignedQuiz = AssignedQuiz::findOrFail($id);
        $assignedQuiz->update($request->get('assignedQuiz'));
        if(!$assignedQuiz->save()){
            return $this->respondUnprocessableEntity();
        }
        return $this->respond([
            'data' => $assignedQuiz
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $assignedQuiz = AssignedQuiz::findOrFail($id);
        $assignedQuiz->delete();
        return $this->respond([
            'message' => 'Quiz successfully deleted'
        ]);
    }
}
