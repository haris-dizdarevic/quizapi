<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
   protected  $fillable = ['id', 'question_id', 'answer_text', 'correct'];
    public $timestamps = false;

    public function savedquizzes(){
        return $this->hasOne('App\Models\SavedQuiz');
    }

    public function question(){
        return $this->belongsTo('App\Model\Question','question_id');
    }
}
