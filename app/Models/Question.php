<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $fillable = ['quiz_id', 'question_text'];
    public $timestamps = false;

    public function answers(){
        return $this->hasMany('App\Models\Answer');
    }

    public function quizzes(){
        return $this->belongsTo('App\Models\Quiz');
    }

    public function savedQuizzes(){
        return $this->hasOne('App\Models\SavedQuiz');
    }
}
