<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SavedQuiz extends Model
{
    protected $fillable = ['answer_id', 'result_id','date'];
    public $timestamps = false;

}
