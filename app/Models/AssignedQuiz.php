<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AssignedQuiz extends Model
{
    protected $fillable = ['user_id', 'student_id', 'quiz_id'];
    public $timestamps = false;

    public function quizzes(){
        return $this->belongsTo('App\Models\Quiz','quiz_id');
    }

}
