<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Quiz extends Model
{
    protected $fillable = ['user_id', 'category_id', 'active', 'date'];
    public $timestamps = false;


    public function questions(){
        return $this->hasMany('App\Models\Question');
    }

    public function results(){
        return $this->hasMany('App\Models\Result');
    }

    public function savedQuizzes(){
        return $this->hasMany('App\Models\SavedQuiz');
    }

    public function assignedQuizzes(){
        return $this->hasMany('App\Models\AssignedQuiz');
    }

    public function category(){
        return $this->belongsTo('App\Models\Category','category_id');
    }

    public function answers(){
        return $this->hasManyThrough('App\Models\Answer','App\Models\Question');
    }
}
