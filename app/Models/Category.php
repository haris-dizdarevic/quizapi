<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['category_name'];
    public $timestamps = false;

    public function quizzes(){
        return $this->hasMany('App\Models\Quiz');
    }
}
