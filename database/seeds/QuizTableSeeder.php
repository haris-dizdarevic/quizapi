<?php

use App\Models\Category;
use App\Models\Quiz;
use App\Models\User;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class QuizTableSeeder extends Seeder{

    public function run(){

        $faker = Faker::create();
        $userIds = User::lists('id');
        $categoryIds = Category::lists('id');
        foreach(range(1,20) as $index){
            Quiz::create([
                'user_id'  => $faker->randomElement($userIds->toArray()),
                'category_id' => $faker->randomElement($categoryIds->toArray()),
                'active' => $faker->boolean(20),
                'date' => $faker->dateTime($max = 'now')
            ]);
        }
    }
}