<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    private  $tables = [
        'users',
        'answers',
        'assigned_quizzes',
        'categories',
        'questions',
        'quizzes',
        'results',
        'saved_quizzes'
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->cleanDatabase();
        Model::unguard();

        $this->call('UserTableSeeder');
        $this->call('CategoryTableSeeder');
        $this->call('QuizTableSeeder');
        $this->call('QuestionTableSeeder');
        $this->call('AnswerTableSeeder');
        $this->call('ResultTableSeeder');
        $this->call('SavedQuizTableSeeder');
        $this->call('AssignedQuizTableSeeder');

        Model::reguard();
    }

    private function cleanDatabase()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        foreach($this->tables as $tableName){
            DB::table($tableName)->truncate();
        }
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
