<?php

use App\Models\Quiz;
use Faker\Factory as Faker;
use App\Models\Result;
use App\Models\User;
use Illuminate\Database\Seeder;

class ResultTableSeeder extends Seeder{

    public function run(){

        $faker = Faker::create();
        $userIds = User::lists('id');
        $quizIds = Quiz::lists('id');

        foreach(range(1,20) as $index){
            Result::create([
                'quiz_id' => $faker->randomElement($quizIds->toArray()),
                'user_id' => $faker->randomElement($userIds->toArray()),
                'score' => $faker->biasedNumberBetween($min = 0, $max = 100)
            ]);
        }
    }
}