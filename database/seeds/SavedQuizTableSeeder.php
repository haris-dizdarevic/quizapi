<?php

use App\Models\Answer;
use App\Models\Question;
use App\Models\Quiz;
use App\Models\Result;
use App\Models\User;
use Faker\Factory as Faker;
use App\Models\SavedQuiz;
use Illuminate\Database\Seeder;

class SavedQuizTableSeeder extends Seeder{

    public function run(){

        $faker = Faker::create();
        $resultIds = Result::lists('id');
        $answerIds = Answer::lists('id');

        foreach(range(1,20) as $index){
            SavedQuiz::create([
                'result_id' => $faker->randomElement($resultIds->toArray()),
                'answer_id' => $faker->randomElement($answerIds->toArray()),
                'date' => $faker->dateTime($max = 'now')
            ]);
        }
    }
}