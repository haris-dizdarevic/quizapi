<?php
use App\Models\Question;
use Faker\Factory as Faker;
use App\Models\Answer;
use Illuminate\Database\Seeder;

class AnswerTableSeeder extends Seeder{

    public function run(){

        $faker = Faker::create();
        $questionIds = Question::lists('id');
        foreach(range(1,60) as $index){
            Answer::create([
                'question_id' => $faker->randomElement($questionIds->toArray()),
                'answer_text' => $faker->sentence(3),
                'correct' => $faker->boolean(30)
            ]);
        }
    }
}