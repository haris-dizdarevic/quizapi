<?php

use Faker\Factory as Faker;
use App\Models\Category;
use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder{

    public function run(){

        $faker = Faker::create();

        foreach(range(1,20) as $index){
            Category::create([
                'category_name' => $faker->sentence(1)
            ]);
        }
    }
}