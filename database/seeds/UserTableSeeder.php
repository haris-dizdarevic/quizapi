<?php

use Faker\Factory as Faker;
use App\Models\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder{

    public function run(){

        $faker = Faker::create();

        foreach(range(1,20) as $index){
            User::create([
                'first_name' => $faker->firstName,
                'last_name'  => $faker->LastName,
                'index_number' => $faker->randomDigit,
                'email' => $faker->email,
                'username' => $faker->userName,
                'password' => $faker->password,
                'admin'=> $faker->boolean(30),
            ]);
        }
    }
}