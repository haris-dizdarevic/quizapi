<?php

use App\Models\Quiz;
use Faker\Factory as Faker;
use App\Models\Question;
use Illuminate\Database\Seeder;

class QuestionTableSeeder extends Seeder{

    public function run(){

        $faker = Faker::create();
        $quizIds = Quiz::lists('id');
        foreach(range(1,20) as $index){
            Question::create([
                'quiz_id' => $faker->randomElement($quizIds->toArray()),
                'question_text' => $faker->sentence(4)
            ]);
        }
    }
}