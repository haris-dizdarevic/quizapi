<?php
use App\Models\Quiz;
use App\Models\User;
use Faker\Factory as Faker;
use App\Models\AssignedQuiz;
use Illuminate\Database\Seeder;

class AssignedQuizTableSeeder extends Seeder{

    public function run(){

        $faker = Faker::create();
        $userIds = User::lists('id');
        $quizIds = Quiz::lists('id');

        foreach(range(1,20) as $index){
            AssignedQuiz::create([
                'user_id' => $faker->randomElement($userIds->toArray()),
                'student_id' => $faker->randomElement($userIds->toArray()),
                'quiz_id' => $faker->randomElement($quizIds->toArray())
            ]);
        }
    }
}