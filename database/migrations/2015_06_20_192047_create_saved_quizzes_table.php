<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSavedQuizzesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('saved_quizzes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('result_id')->unsigned()->index();
            $table->integer('answer_id')->unsigned()->index();
            $table->dateTime('date');
        });

        Schema::table('saved_quizzes', function($table) {
            $table->foreign('answer_id')->references('id')->on('answers')->onDelete('cascade');
            $table->foreign('result_id')->references('id')->on('results')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('saved_quizzes');
    }
}
